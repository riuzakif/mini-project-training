/*
  Warnings:

  - You are about to drop the `product` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `product_images` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `product_taxonomy` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `taxonomy` table. If the table is not empty, all the data it contains will be lost.

*/
-- DropForeignKey
ALTER TABLE `product_images` DROP FOREIGN KEY `product_images_productId_fkey`;

-- DropForeignKey
ALTER TABLE `product_taxonomy` DROP FOREIGN KEY `product_taxonomy_productId_fkey`;

-- DropForeignKey
ALTER TABLE `product_taxonomy` DROP FOREIGN KEY `product_taxonomy_taxonomyId_fkey`;

-- DropTable
DROP TABLE `product`;

-- DropTable
DROP TABLE `product_images`;

-- DropTable
DROP TABLE `product_taxonomy`;

-- DropTable
DROP TABLE `taxonomy`;

-- CreateTable
CREATE TABLE `document` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `title` VARCHAR(150) NOT NULL,
    `code` VARCHAR(150) NOT NULL,
    `description` LONGTEXT NOT NULL,
    `createdAt` TIMESTAMP(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
    `updatedAt` TIMESTAMP(0) NULL DEFAULT CURRENT_TIMESTAMP(0),
    `deletedAt` DATETIME(3) NULL,

    INDEX `index_document_id`(`id`),
    INDEX `index_document_code`(`code`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
