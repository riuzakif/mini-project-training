import Link from 'next/link'
export function Navbar() {
    return (
        <>
            <nav className="navbar navbar-dark bg-dark">
                <a className="navbar-brand pl-4" href="/dashboard">Mini Project Farhan</a>
                <nav className="nav pr-4">
                    <a className="text-white nav-link " href="/profile">My Profile</a>
                    <a className="text-white nav-link " href="/quotes">Quotes of the Day</a>
                    <a className="text-white nav-link " href="/document">List Document</a>
                </nav>
            </nav>
        </>
    )
};

