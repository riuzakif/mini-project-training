import Joi from 'joi';
import validator from './default.validator'

export { validator };
const create = validator({
    body: Joi.object({
        title: Joi.string().required(),
        code: Joi.string().required(),
        description: Joi.string().required(),
    })
})

const DocumentValidator = {
    create
}

export { DocumentValidator }

