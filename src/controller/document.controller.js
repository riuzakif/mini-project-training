import {PrismaClient} from "@prisma/client";

export default class DocumentController{
    constructor(props) {
        this.prisma = new PrismaClient()
        this.fields = props?.fields ?? null
        this.key = props?.key ?? null
        this.value = props?.value ?? null
    }

    async create(){
        try {
            if(!this.fields) return [new Error('fields is required'), null]

            const  result = await this.prisma.document.create({
                data: this.fields
            })
            return [null, result]
        }catch (err){
            return [err,null]
        }
    }

    async delete(){
        try {
            if(!this.key && !this.value) return [new Error('key & value is required'), null]

            const deleted = await this.prisma.document.delete({
                where: {
                    [this.key]: this.value
                }
            })
            return [null, deleted]
        }catch (e) {
            return [e,null]
        }
    }

    async getAll(){
        try {
            const document = await this.prisma.document.findMany();

            return [null, document];
        } catch (err) {
            return [err, null];
        }
    }
}