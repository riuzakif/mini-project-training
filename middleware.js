import {NextResponse} from 'next/server'
import {getToken} from "next-auth/jwt";

export async function middleware(req){
    const token = await getToken({
        req,
        secret: process.env.NEXTAUTH_secret,
        secureCookie: false
    })

    if(req.nextUrl.pathname.startsWith('/auth') && token){
        return NextResponse.redirect(new URL('/dashboard', req.url))
    }
    if(req.nextUrl.pathname.startsWith('/dashboard') && !token){
        return NextResponse.redirect(new URL("/auth/login", req.url))
    }
    if(req.nextUrl.pathname.startsWith('/profile') && !token){
        return NextResponse.redirect(new URL("/auth/login", req.url))
    }
    if(req.nextUrl.pathname.startsWith('/quotes') && !token){
        return NextResponse.redirect(new URL("/auth/login", req.url))
    }

    return NextResponse.next()
}

export const config = {
    matcher:[
        '/dashboard',
        '/dashboard/:path*',
        '/auth/:path*',
        '/profile/',
        '/profile/:path*',
        '/quotes/',
        '/quotes/:path*'
    ]
}