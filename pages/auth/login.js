import { useFormik } from 'formik'
import { signIn } from 'next-auth/react'
import * as Yup from 'yup'
import {useRouter} from "next/router";

export default function Login() {
    const router = useRouter();

    const formik = useFormik({
        initialValues: {
            email: '',
            password: ''
        },
        validationSchema: Yup.object({
            email: Yup.string()
                .max(30, 'Must be 30 characters or less')
                .email('Invalid Email Address')
                .required('please enter your email'),
            password: Yup.string()
                .required('please enter your password')
        }),
        onSubmit: async (value) => {
            // console.log({ value });
            const credentials = await signIn(
                'credentials',
                {
                    email: value?.email,
                    password: value?.password,
                    redirect: false
                }
            )

            if (credentials.ok){
                router.push(
                    '/dashboard'
                )
            }
            // return {
            //     ...credentials
            // }
        }

    })
    return (
        <section className="bg-gray-50 dark:bg-gray-900">
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <div className={'container'}>
                    <div className={'row'}>
                        <div className={'col-md-4 offset-4'}>
                            <h4 className={'text-center'}>Farhan Apps</h4>
                            <div className={'card'}>
                                <div className={'card-body'}>
                                    <form className={'space-y-4 md:space-y-6'} onSubmit={formik.handleSubmit}>
                                        <h6 className={''}>Sign in to your account</h6>
                                        <div className={'form-group'}>
                                            <label htmlFor="">Email</label>
                                            <input
                                                className='form-control'
                                                type='email'
                                                name={'email'}
                                                value={formik?.values?.email}
                                                onChange={formik.handleChange}
                                                placeholder={'input your email'}
                                            ></input>
                                            {
                                                formik.errors &&
                                                formik.touched &&
                                                formik.errors?.email &&
                                                formik.touched?.email &&
                                                (
                                                <span className={'!text-red-500 !text-xs'}>{formik.errors?.email}</span>
                                                )
                                            }
                                        </div>
                                        <div className={'form-group '}>
                                            <label htmlFor="">Password</label>
                                            <input
                                                className='form-control'
                                                type='password'
                                                name={'password'}
                                                value={formik?.values?.password}
                                                onChange={formik.handleChange}
                                                placeholder={'input your password'}
                                            ></input>
                                            {
                                                formik.errors &&
                                                formik.touched &&
                                                formik.errors?.password &&
                                                formik.touched?.password &&
                                                (
                                                    <span className={'!text-red-500 !text-xs'}>{formik.errors?.password}</span>
                                                )
                                            }
                                        </div>

                                        <button
                                        type={'submit'}
                                        className={'w-full !text-white !bg-blue-600 hover:!bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'}
                                        >
                                        Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
};
