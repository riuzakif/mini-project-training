export default function SsrOld({data}) {
    // console.log(params);
    return (
        <>
            <h1>Server Side Rendering</h1>
            <p>simulasi server side rendering</p>

            <div className={'w-full space-y-6'}>
                {
                    data.map((item) => {
                        return (
                            <div className={'w-full bg-gray-100 p-4'}>
                                <p>{item.userId}</p>
                                <p>{item.id}</p>
                                <p>{item.title}</p>
                                <p>{item.body}</p>
                            </div>
                        )
                    })
                }
            </div>
        </>
    )
};

export async function getServerSideProps() {
    let data = []

    await fetch('https://jsonplaceholder.typicode.com/posts')
        .then((response) => response.json())
        .then((response) => {
            data = response
        })
        .catch((err) => {
            data = []
        })

    return {
        props: {
            data
        }
    }
};
