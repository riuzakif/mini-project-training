import axios from "axios"
import {Navbar} from "@/src/components/navbar.component";
import { useEffect, useState } from 'react';

export default function profile() {
    const [data, setData] = useState([]);
    const [visible, setVisible] = useState(true);

    useEffect(() => {
        if (visible) {
            fetch('https://mocki.io/v1/2e758462-60f2-46f6-8670-675826527e52').then((res) => res.json()).then((result) => {
                setData(result);
            }).catch((err) => {
            })
        }
    }, [visible]);

    return (
        <section className="bg-gray-50 dark:bg-gray-900">
            <Navbar></Navbar>
            <div className={'container pt-10'}>
                <h4 className={'py-2'}>About Me</h4>
                <div className={'card'}>
                    <div className={'card-body'}>
                        <h3 className={'py-2'}>{data.name}</h3>
                        <div className={'row border-b-2'}>
                            <div className={'col'}><h6>{data.email}</h6></div>
                            <div className={'col'}><h6>{data.phone}</h6></div>
                            <div className={'col'}><h6>{data.address}</h6></div>
                        </div>
                        <h6 className={'py-4'}>{data.about}</h6>
                    </div>
                </div>
            </div>
        </section>
    )
};
