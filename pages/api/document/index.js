import ErrorHandler from '@/src/handlers/error.handler'
import { DocumentValidator } from '@/src/validator'
import nc from 'next-connect'
import UserController from "@/src/controller/user.controller";
import DocumentController from "@/src/controller/document.controller";

const handler = nc(ErrorHandler)

handler
    .post(
        DocumentValidator.create,
        async (req, res) => {
            let recordData = req.body;
            // create record
            const [err,data] = await new DocumentController({
                fields: recordData
            }).create()

            if(err){
                return res.status(400).json({
                    message: err?.message ?? "Error: Some Error"
                })
            }

            return res.status(200).json({
                message: 'OK!',
                data: data
            })
        })
    .get(async (req,res)=>{
        const [err, data] = await new DocumentController().getAll();

        return res.status(201).json({
            rc: "00",
            data: data
        })
    })

export default handler