import {Navbar} from "@/src/components/navbar.component";
import { useFormik } from 'formik'
import { signIn } from 'next-auth/react'
import * as Yup from 'yup'
import {useRouter} from "next/router";
import axios from "axios";

export default function Dashboard(){
    const router = useRouter();

    const formik = useFormik({
        initialValues: {
            title: '',
            code: '',
            description: ''
        },
        validationSchema: Yup.object({
            title: Yup.string()
                .required('please enter your title'),
            code: Yup.string()
                .required('please enter your code'),
            description: Yup.string()
                .required('please enter your description')
        }),
        onSubmit: async (values) => {
            axios.post('http://localhost:3100/api/document', {
                title: values?.title,
                code: values?.code,
                description: values?.description,
            }).then(async (response) => {
                router.push(
                    '/document'
                )
            }).catch(async (err) => {
                router.push(
                    '/document/create'
                )
            })
        }

    })

    return(
        <section className="bg-gray-50 dark:bg-gray-900">
            <Navbar></Navbar>
            <div className={'container pt-10'}>
                <h4 className={'py-2'}>Add Document</h4>
                <div className={'card'}>
                    <div className={'card-body'}>
                        <form className={'space-y-4 md:space-y-6'} onSubmit={formik.handleSubmit}>
                            <div className={'form-group'}>
                                <label htmlFor="">title</label>
                                <input
                                    className='form-control'
                                    type='text'
                                    name={'title'}
                                    value={formik?.values?.title}
                                    onChange={formik.handleChange}
                                    placeholder={'input your title'}
                                ></input>
                                {
                                    formik.errors &&
                                    formik.touched &&
                                    formik.errors?.title &&
                                    formik.touched?.title &&
                                    (
                                        <span className={'!text-red-500 !text-xs'}>{formik.errors?.title}</span>
                                    )
                                }
                            </div>
                            <div className={'form-group'}>
                                <label htmlFor="">code</label>
                                <input
                                    className='form-control'
                                    type='text'
                                    name={'code'}
                                    value={formik?.values?.code}
                                    onChange={formik.handleChange}
                                    placeholder={'input your code'}
                                ></input>
                                {
                                    formik.errors &&
                                    formik.touched &&
                                    formik.errors?.code &&
                                    formik.touched?.code &&
                                    (
                                        <span className={'!text-red-500 !text-xs'}>{formik.errors?.code}</span>
                                    )
                                }
                            </div>
                            <div className={'form-group'}>
                                <label htmlFor="">description</label>
                                <input
                                    className='form-control'
                                    type='text'
                                    name={'description'}
                                    value={formik?.values?.description}
                                    onChange={formik.handleChange}
                                    placeholder={'input your description'}
                                ></input>
                                {
                                    formik.errors &&
                                    formik.touched &&
                                    formik.errors?.description &&
                                    formik.touched?.description &&
                                    (
                                        <span className={'!text-red-500 !text-xs'}>{formik.errors?.description}</span>
                                    )
                                }
                            </div>
                            <button
                                type={'submit'}
                                className={'w-full !text-white !bg-blue-600 hover:!bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800'}
                            >
                                Submit
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    )
}