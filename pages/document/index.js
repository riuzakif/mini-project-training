import {Navbar} from "@/src/components/navbar.component";
import axios from "axios";

export default function Dashboard(props){
    return(
        <section className="bg-gray-50 dark:bg-gray-900">
            <Navbar></Navbar>
            <div className={'container pt-10'}>
                <div className={'row'}>
                    <div className={'col-md-6'}>
                        <h4 className={'py-2'}>List Document</h4>
                    </div>
                    <div className={'col-md-6'}>
                        <a href="/document/create" className={'btn btn-primary btn-sm float-right'}>Add New Record</a>
                    </div>
                </div>
                <div className={'card'}>
                    <table className={'table table-bordered'}>
                        <thead class={'text-center font-bold'}>
                        <tr>
                            <th className={'w-1'}>ID</th>
                            <th>Quote</th>
                            <th>Author</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    )
}

export async function getServerSideProps() {
    const [err, data]= await axios
        .get('https://localhost:3100/api/document')
        .then((res) => {
            console.log(res)

            return [null, res.data]
        })
        .catch((err) => {
            return [err,null]
        })
    console.log(err)
    return
    if (err) {
        return {
            redirect: {
                destination: '/dashboard',
                permanent: false
            }
        }
    }
    return {
        props: {
            data
        }
    }
}
