import "@/src/assets/sass/main.scss";
import "@/src/assets/dist/styles.css";

import { useRouter } from "next/router";
import ProductLayout from "@/src/components/product.layout";
import {SessionProvider, getSession} from 'next-auth/react'
import { useEffect } from 'react';

function AppSwitchingTheme({ children}) {
  const router = useRouter()
  if (router.asPath.startsWith('/product')) {
    return (
      <ProductLayout>
        {children}
      </ProductLayout>
    )
  } else {
    return children
  }
}
function App(props) {
  useEffect(() => {
    import('bootstrap/dist/js/bootstrap');
  }, []);

  let {
    Component,
    pageProps: {
      session,
      ...pageProps
    }
  } = props

  // console.log(session, 'SESSION');
  
  return (
    <SessionProvider session={session}>
      <AppSwitchingTheme>
        <Component {...pageProps} />
      </AppSwitchingTheme>
    </SessionProvider>
  )
}

App.getInitialProps = async ({ Component, ctx }) => {
  let pageProps = Component.getInitialProps ? await Component.getInitialProps(ctx) : {};
  const session = await getSession(ctx);

  pageProps = {
    ...pageProps,
    session
  }

  return { pageProps }
}

export default App