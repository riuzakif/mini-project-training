import {Navbar} from "@/src/components/navbar.component";

export default function Dashboard(){
    return(
        <section className="bg-gray-50 dark:bg-gray-900">
            <Navbar></Navbar>
            <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
                <h1>Welcome to main Page</h1>
            </div>
        </section>
    )
}