import axios from "axios"
import {Navbar} from "@/src/components/navbar.component";

export default function ssr({data}) {
    return (
        <section className="bg-gray-50 dark:bg-gray-900">
            <Navbar></Navbar>
            <div className={'container pt-10'}>
                <h4 className={'py-2'}>Quotes of the Day</h4>
                <div className={'card'}>
                        <table className={'table table-bordered'}>
                            <thead class={'text-center font-bold'}>
                            <tr>
                                <th className={'w-1'}>ID</th>
                                <th>Quote</th>
                                <th>Author</th>
                            </tr>
                            </thead>
                            <tbody>
                            {
                                Array.isArray(data) && data.length > 0 ?
                                    data.map((item) => {
                                        return (
                                            <tr>
                                                <td className={'text-center'}>{item.id }</td>
                                                <td>{item.quote }</td>
                                                <td>{item.author }</td>
                                            </tr>
                                        )
                                    })
                                    : ''
                            }
                            </tbody>
                        </table>
                </div>
            </div>
        </section>
    )
};

export async function getServerSideProps() {
    const [err, data]= await axios
        .get('https://dummyjson.com/quotes')
        .then((res) => {
            return [null, res.data.quotes]
        })
        .catch((err) => {
            return [err,null]
        })

    if (err) {
        return {
            redirect: {
                destination: '/about',
                permanent: false
            }
        }
    }
    return {
        props: {
            data
        }
    }
}