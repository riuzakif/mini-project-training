import {useState} from 'react'
import { Navbar } from '@/src/components/navbar.component'
import Button from "@/src/components/button.component";
import Modal from "@/src/components/modal.component";
import Image from 'next/image'

export default function Home() {
  const [visible, setVisible] = useState(false)
  
  function onChangeModal() {
    setVisible(!visible)
  }
  
  return (
      <section className="bg-gray-50 dark:bg-gray-900">
          <div className="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
            <h1>Welcome to Farhan mini project</h1>
              <h4>Log in account to continue</h4>
              <a className={'btn btn-success btn-sm'} href="/auth/login">Login</a>
          </div>
      </section>
  )
}