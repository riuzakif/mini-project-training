export default function ProductDetail(props) {

    return (
        <h1>Product Detail { props?.productId}</h1>
    )
};

export async function getServerSideProps(context) {
    let { productId } = context.params
    // console.log(context.params);

    return {
        props: {
            productId
        }
    }
}