import { Navbar } from '@/src/components/navbar.component'
import { useState, useEffect } from 'react'

export default function ProductList() {
    const [visible, setVisible] = useState(false)
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        setData([])
        if (visible) {
            setLoading(true)        
            fetch('https://jsonplaceholder.typicode.com/posts')
                .then((res) => res.json())
                .then((result) => {
                    setTimeout(() => {
                        setData(result)
                        setLoading(false)
                    }, 4000);
                    clearTimeout()
                })
                .catch((err)=> {
                    setLoading(false)
                })
        }
    }, [visible])
    console.log(loading, 'LOADING');
    let timeout
    // useEffect(() => {
    //     setLoading(true)
    //     timeout = setTimeout(() => {
    //         setData([{ id: 1 }, { id: 2 }])
    //         setLoading(false)
    //     }, 3000)
    //     return () => clearTimeout(timeout)
    // })

    return (
        <div>
            {/* <Navbar /> */}
            <h1>Product List</h1>
            <button
                className={'bg-blue-400 text-white rounded-full pr-2 pl-2'}
                onClick={() => setVisible(!visible)}
            >
                {visible ? 'Ok' : 'Not Ok'}
            </button>
            {
                loading ? 'loading'
                    :
                    data.map((item) => {
                        return (
                            <div className={'w-full bg-slate-200 my-4 p-4'}>
                                <p>{item.userId}</p>
                                <p>{item.id}</p>
                                <p>{item.title}</p>
                                <p>{item.body}</p>
                            </div>
                        )
                    })
            }
        </div>
    )
}